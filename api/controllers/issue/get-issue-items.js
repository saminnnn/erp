module.exports = async function getIssueItems(req, res) {
  const item = req.query.item;
  const brand = req.query.brand;
  const sql = `SELECT * FROM itemstock($1, $2)`;
  const searchData = await sails.sendNativeQuery(sql, [brand, item]);
  res.send(searchData.rows);
}