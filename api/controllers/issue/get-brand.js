module.exports = async function getBrand(req, res) {
  const item = req.query.item;
  const receives = await Receive.find({status: {'>=': 0}, itemid: item});
  const brands = new Set(receives.map(e => e.brand))
  res.send(Array.from(brands));
}