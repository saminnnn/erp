module.exports = async function updateIssue(req, res) {
  const id = req.body.id;
  const data = req.body.data;

  if(!id) {
      let serial;
      const sql = 'SELECT issueno FROM issue ORDER BY issueid DESC LIMIT 1';
      const rawResult = await sails.sendNativeQuery(sql, []);
      
      if(rawResult.rowCount === 0) serial = 1;
      else {
          const issue = rawResult.rows[0].issueno;
          serial = Number.parseInt(issue.substring(4, 8)) + 1;
      }

      let serialString = serial + '';
      let length = 4 - serialString.length;
      for(let i=0; i < length; i++) {
          serialString = '0' + serialString;
      }

      length = data.length;
      for(let i=0; i < length; i++) {
          const element = data[i];          
          element.issueno = 'ISS-' + serialString;
          await Issue.create(element);
          let receiveStatus;

          element.total == element.quantity? receiveStatus = -1: receiveStatus = 1;
          await Receive.update({batch: element.batch, itemid: element.itemid, brand: element.brand}).set({status: receiveStatus});

          res.send({update: true});
      };
  }
  else {
      await Issue.update({id: id}).set(data);
      res.send({update: true});
  }
}