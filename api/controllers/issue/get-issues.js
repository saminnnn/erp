module.exports = async function getIssue(req, res) {
  const issues = await Issue.find({sort: [{id: 'DESC'}]})
  res.send(issues);
}