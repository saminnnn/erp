module.exports = async function paymentReport(req, res) {
  const fromDate = req.query.fromDate;
  const toDate = req.query.toDate;
  let company = req.query.company;
  if(!company) company = 0;

  const sql = `select * from public.payment_report($1,$2, $3);` ;
  const searchData = await sails.sendNativeQuery(sql, [fromDate, toDate, company]);
  res.send(searchData.rows);
}