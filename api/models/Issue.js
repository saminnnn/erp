
module.exports = {
  attributes: {
      id: {type: 'number', columnName: 'issueid', autoIncrement: true}, 
      item: {type: 'string', allowNull: false}, itemid: {type: 'number', allowNull: false}, date: { type: 'ref', columnType: 'datetime' }, 
      batch: {type: 'string'}, price: {type: 'string', allowNull: false}, quantity: {type: 'string', allowNull: false}, unit: {type: 'string', allowNull: false},
      employee: {type: 'string'}, employeeid: {type: 'number'},
      company: {type: 'string'}, companyid: {type: 'number'}, brand: {type: 'string'},
      remark: {type: 'string'}, issueno: {type: 'string', allowNull: false}
  },
  tableName: 'issue'
}




/**
 * CREATE TABLE public.issue
(
    issueid bigserial,
    item text NOT NULL,
    itemid integer NOT NULL,
    date date NOT NULL,
    quantity numeric,
    price numeric,
    unit text,
    batch text,
    company text,
    companyid integer,
    employee text,
    employeeid integer,
    remark: text
    PRIMARY KEY (issueid)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.issue
    OWNER to samin;
 */