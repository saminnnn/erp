function addCell(row, data) {
    const cell = row.insertCell();
    cell.append(data);
}

function insertCustomCell(row, html) {
    const cell = row.insertCell();
	cell.innerHTML = html;
}

function tableHello() {
    console.log('HELLO');
}